/*jslint browser: true, indent: 2 */
/*global $, jQuery, ctwitter*/

$(document).ready(function () {
  "use strict";

  var streamStore, twitter = new ctwitter.CTwitter(), terms = [], value;

  // Protect from XSS (at least partially)
  function sanitize(text) {
    return text.replace(/</g, '&lt;').replace(/"/g, '&quot;');
  }

  function beginStream() {
    twitter.stream("statuses/filter", {
      lang: "en",
      track: terms
    }, function (stream) {
      streamStore = stream;
      stream.on("data", function (tweet) {
        $(".tweets").prepend("<p class='tweet'>" + tweet.text + "</p>");
        $(".tweet").fadeIn(800, function () {
          // Select random tweets to spoiler alert
          // This could also be applied to tweets that have keywords
          //   that you don't want displayed (such as expletives)
          if (Math.floor((Math.random() * 10)) > 8) {
            $(this).spoilerAlert();
          }
        });

        if ($(".tweet").size() >= 10) {
          $(".tweet:last").fadeOut(400, function () {
            $(this).remove();
          });
        }
      });
    });
  }

  $(".search input[type=text]").keypress(function (e) {
    // If enter(13) is pressed and input text field isn't empty
    if ((e.which === 13) && $(this).val() !== "") {
      value = sanitize($(this).val());
      terms.push(value);
      $(".search #terms").append("<span class='term'>" + value + "</span>");
      $(this).val("");

      // Check to see if beginStream has been called before
      if (!streamStore) {
        beginStream();
      } else {
        // Destroy the existing stream
        streamStore.destroy();
        // Call a new stream
        beginStream();
      }
    }
  });
});
